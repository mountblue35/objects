
function cb(input,key,value){

    if(typeof(value) == 'number'){
        return (input[key] + 5);
    }
    if(typeof(value) == 'string'){
        return (input[key] + "works");
    }
    
}

function mapObject(input_object={},cb){
    if (input_object !== {}){        
        
        for(let index in input_object){

            input_object[index] = cb(input_object,index,input_object[index]);

        }
        return input_object;

    }else{
        return [];
    }

}

module.exports.cb = cb;
module.exports.mapObject = mapObject;
