
// 
function pairs(input_object={}){
    if (input_object !== {}){

        let keys = [];
        
        for(let index in input_object){

            keys.push(index);
        
        }

        let len = keys.length;
        
        let pairs = Array(len);

        for (let index2 = 0; index2 < len; index2++){

            
            pairs[index2] = [ keys[index2], input_object[ keys[index2] ] ];
        
        }
        return pairs;

    }else{
        return [];
    }

}



module.exports = pairs;
