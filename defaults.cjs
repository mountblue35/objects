
// 



function defaults(input_object={}, defaultProps){

    if (input_object !== {}){

        let result_object = input_object;
        for(let index in defaultProps){
            if(!(index in input_object)){
                result_object[index] = defaultProps[index];
            }
        }
        
        return result_object;

    }else{
        return {};
    }

}



module.exports = defaults;
