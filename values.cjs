
// 



function values(input_object={}){
    if (input_object !== {}){

        let arr = [];
        
        for(let index in input_object){
            
            let temp_val = input_object[index];
        
            if(typeof(temp_val) !== 'function'){
        
                arr.push(temp_val);
            }
        }
        return arr;

    }else{
        return [];
    }

}



module.exports = values;
